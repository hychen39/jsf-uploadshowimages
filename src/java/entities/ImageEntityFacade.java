/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *  @version %I%, %G%
 */
package entities;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author hychen39@gmail.com
 */
@Stateless
public class ImageEntityFacade extends AbstractFacade<ImageEntity> {

    @PersistenceContext(unitName = "UploadImagePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ImageEntityFacade() {
        super(ImageEntity.class);
    }
    
}
