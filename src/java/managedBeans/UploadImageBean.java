/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *  @version %I%, %G%
 */
package managedBeans;

import entities.ImageEntity;
import entities.ImageEntityFacade;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.apache.commons.io.IOUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author hychen39@gmail.com
 */
@Named(value = "uploadImageBean")
@SessionScoped
public class UploadImageBean implements Serializable {

    private UploadedFile file;
    private StreamedContent image;

    public StreamedContent getImage() {
        image = showImage();
        return image;
    }

    public void setImage(StreamedContent image) {
        this.image = image;
    }

    @EJB
    ImageEntityFacade imageEntityFacade;

    /**
     * Creates a new instance of UploadImageBean
     */
    public UploadImageBean() {
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    /**
     * FileUploadEvent handler for uploading file.
     */
    public void handleFileUpload(FileUploadEvent event) throws IOException {
        file = event.getFile();
        byte[] content = IOUtils.toByteArray(file.getInputstream());

        ImageEntity imageEntity = new ImageEntity();
        imageEntity.setPhoto(content);
        imageEntityFacade.create(imageEntity);

        FacesMessage message = new FacesMessage("Succesful",
                file.getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, message);

    }

    public StreamedContent showImage() {
        return showImage(new Long(1));
    }

    /**
     * Show a image by using p:graphicImage. Reference:
     * http://stackoverflow.com/questions/10944673/how-to-use-pgraphicimage-with-defaultstreamedcontent-in-an-uirepeat.
     *
     * @param id
     * @return
     */
    public StreamedContent showImage(Long id) {
        ImageEntity imageEntity = imageEntityFacade.find(id);
        return new DefaultStreamedContent(
                new ByteArrayInputStream(imageEntity.getPhoto()));
    }

    public StreamedContent showImageByParam01() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        System.out.print("ResourceHandler: " + facesContext.getCurrentPhaseId() + " " + facesContext.isPostback());
        String idStr = (String) facesContext.getExternalContext().getRequestParameterMap().get("id");
        if (idStr != null) {
            ImageEntity imageEntity = imageEntityFacade.find(Long.valueOf(idStr));
            if (imageEntity != null) {
                return new DefaultStreamedContent(
                        new ByteArrayInputStream(imageEntity.getPhoto()));
            } else {
                return new DefaultStreamedContent();
            }
        } else {
            return new DefaultStreamedContent();
        }
    }

    /**
     * Show image by using f:param. Reference:
     * {@link http://stackoverflow.com/questions/10944673/how-to-use-pgraphicimage-with-defaultstreamedcontent-in-an-uirepeat.}
     *
     * @return
     */
    public StreamedContent showImageByParam() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        if (facesContext.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            System.out.print("Return StreamedContent Stub" + facesContext.getCurrentPhaseId() + " " + facesContext.isPostback());
            // So, we're rendering the view. 
            //Return a stub StreamedContent so that it will generate right URL.
            // The StreamedContent objects are placed in the session scope with 
            // an encrypted key.
            // After the rendering phase, 
            // the key is appended to image url that points to JSF resource handler.
            return new DefaultStreamedContent();
        } else {
            // Second request made by custom PrimeFaces ResourceHandler
            // 
            System.out.print("ResourceHandler: " 
                    + facesContext.getCurrentPhaseId() 
                    + " " + facesContext.isPostback());
            // So, browser is requesting the image. 
            // Get ID value from actual request param.
            String idStr = (String) facesContext.getExternalContext().getRequestParameterMap().get("id");
            StreamedContent sc = new DefaultStreamedContent();
            if (idStr != null) {
                ImageEntity imageEntity = imageEntityFacade.find(Long.valueOf(idStr));
                if (imageEntity != null) {
                    sc = new DefaultStreamedContent(
                            new ByteArrayInputStream(imageEntity.getPhoto()));
                }
            }
            return sc;
        }
    }

    public List<StreamedContent> getAllStreamedContents() {
        List<ImageEntity> allImages = imageEntityFacade.findAll();
        List<StreamedContent> allContents = new ArrayList<>();
        for (ImageEntity entity : allImages) {
            allContents.add(new DefaultStreamedContent(
                    new ByteArrayInputStream(entity.getPhoto())));
        }

        return allContents;
    }

    public List<ImageEntity> getAllImages() {
        return imageEntityFacade.findAll();
    }

}
